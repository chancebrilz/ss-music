FROM node:12

# Create application directory
WORKDIR /home/node/ppc

# Copy project source
COPY . .

# Install deps
RUN npm ci

# Install serve globally
RUN npm i -g serve

# Build project
RUN npx parcel build src/index.html

# Copy serve.json config
RUN cp serve.json dist/

# Open port
EXPOSE 5000

CMD ["serve", "dist/"]

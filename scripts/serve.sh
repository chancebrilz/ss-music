#!/bin/sh

# Serve the application in a Docker container.
port="${PORT-3000}"

image_name="cbrilz/ss-music-app"

this_script_dir="$(cd "$(dirname "$0")" && pwd)"
project_dir="$this_script_dir/.."

# Build image
docker build -t $image_name $project_dir

# Run detached,
container=$(docker run -p $port:5000 -d $image_name)

echo "\n\nContainer ${container} created.\n\nYou can access the built application at http://localhost:${port}\n"


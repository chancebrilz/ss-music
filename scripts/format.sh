#!/bin/sh

# Format source code.

set -e

this_script_dir="$(cd "$(dirname "$0")" && pwd)"
project_dir="$this_script_dir/.."

`npm bin`/prettier --write '*.json' "$project_dir/src/**/*.{ts,tsx,json}"

exit 0

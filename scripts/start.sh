#!/bin/sh

# Start local development server

set -e

this_script_dir="$(cd "$(dirname "$0")" && pwd)"

cd $this_script_dir/..

`npm bin`/parcel src/index.html
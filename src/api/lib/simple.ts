import jsonp from "jsonp";

export type SimpleGetResponse<Response> =
  | { success: false }
  | { success: true; response: Response };

/**
 * Create and send GET JSONP request, resolve JSON response.
 */
export const simple_jsonp_request = async <Response = object>(
  endpoint: string
): Promise<SimpleGetResponse<Response>> => {
  const request = (): Promise<Response> =>
    new Promise((res, rej) => {
      jsonp(endpoint, { timeout: 5000 }, (err, data) => {
        if (err) rej(err);
        res(data);
      });
    });

  try {
    const response = await request();

    return {
      response,
      success: true,
    };
  } catch (e) {
    return {
      success: false,
    };
  }
};

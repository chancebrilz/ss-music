export type Query = { [key: string]: string | number };

// Helper used to stringify an object into a query string.
const stringify_query = (query: Query) => {
  const parts = Object.entries(query).map(
    ([key, value]) => `${encodeURIComponent(key)}=${encodeURIComponent(value)}`
  );

  return parts.join("&");
};

// Helper used to construct api endpoints.
export const get_endpoint = (host: string, path: string, query?: Query) => {
  const qs = query ? `?${stringify_query(query)}` : "";

  return `${host}${path}${qs}`;
};

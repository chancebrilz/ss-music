import { get_endpoint } from "./lib/helpers";
import { simple_jsonp_request } from "./lib/simple";

import type { MusicAPI, MusicAPIResult } from "~/types";

/**
 * Music API request post-processor
 *
 * NOTE: We must use JSONP here due to the server's
 * CORS policy.
 */
const music_api_call = async (url: string): Promise<MusicAPIResult> => {
  const result = await simple_jsonp_request<{
    resultCount?: number;
    results?: object[];
  }>(url);

  if (!result.success) {
    return {
      success: false,
      error: "Failed to load results.",
    };
  }

  return {
    success: true,
    response: result.response,
  };
};

/**
 * iTunes Search API Wrapper
 *
 * @see https://affiliate.itunes.apple.com/resources/documentation/itunes-store-web-service-search-api/
 */
export const make_music_api = (): MusicAPI => {
  const API_HOSTNAME = "https://itunes.apple.com";

  return {
    search: async params =>
      music_api_call(get_endpoint(API_HOSTNAME, "/search", { ...params })),

    lookup: async params =>
      music_api_call(get_endpoint(API_HOSTNAME, "/lookup", { ...params })),
  };
};

interface MusicAPISuccessResult {
  success: true;
  response: object;
}

interface MusicAPIErrorResult {
  success: false;
  error: string;
}

export type MusicAPIResult = MusicAPISuccessResult | MusicAPIErrorResult;

export interface MusicAPI {
  search: (params: {
    term: string;
    attribute?: string;
    entity?: string;
    limit?: number;
  }) => Promise<MusicAPIResult>;

  lookup: (params: { id: number }) => Promise<MusicAPIResult>;
}

export interface Album {
  wrapperType: string;
  collectionType: string;
  artistId: number;
  collectionId: number;
  amgArtistId: number;
  artistName: string;
  collectionName: string;
  collectionCensoredName: string;
  artistViewUrl: string;
  collectionViewUrl: string;
  artworkUrl60: string;
  artworkUrl100: string;
  collectionPrice: number;
  collectionExplicitness: string;
  trackCount: number;
  copyright: string;
  country: string;
  currency: string;
  releaseDate: string;
  primaryGenreName: string;
}

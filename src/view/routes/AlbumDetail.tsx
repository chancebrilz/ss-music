import * as Preact from "preact";
import { route } from "preact-router";
import { useEffect, useState } from "preact/hooks";
import { useMusicApi } from "~/hooks/music-api";
import { AlbumArt } from "../components";
import { ListenOnApple } from "../components/ListenOnApple";

import type { Album } from "~/types";

export const AlbumDetail = ({ id }: { id: string }) => {
  const do_lookup = useMusicApi("lookup");

  const [state, setState] = useState<{
    album: Album | undefined;
  }>({
    album: undefined,
  });

  useEffect(() => {
    do_lookup({ id: +id }).then(result => {
      if (result.success) {
        const album = result.response["results"]?.[0];

        if (typeof album === "object" && album["collectionType"] === "Album") {
          setState({ album });
          return;
        }
      }

      // TODO: Else, if error or data cannot be read, display error page.
      return route("/");
    });
  }, [id]);

  if (!state.album) {
    return <div class="loading">Loading...</div>;
  }

  return (
    <div class="album-detail">
      <section class="album-detail--header">
        <div class="album-detail--header-album">
          <AlbumArt album={state.album} />
        </div>
        <div class="album-detail--header-info">
          <h2 class="album-name">{state.album.collectionName}</h2>
          <div class="artist-name">{state.album.artistName}</div>
          <div className="view-link">
            <a href={state.album.collectionViewUrl} target="_blank">
              <ListenOnApple />
            </a>
          </div>
        </div>
        <div
          class="album-detail--header-bg"
          style={{
            backgroundImage: `url(${state.album.artworkUrl100})`,
          }}
        />
      </section>
      <section>
        <h3>Metadata</h3>
        <div class="album-detail--body">
          <div class="metadata">
            <h4>Released</h4>
            <span>{new Date(state.album.releaseDate).getFullYear()}</span>
          </div>
          <div class="metadata">
            <h4>Track Count</h4>
            <span>{state.album.trackCount}</span>
          </div>
          <div class="metadata">
            <h4>Genre</h4>
            <span>{state.album.primaryGenreName}</span>
          </div>
        </div>
      </section>
    </div>
  );
};

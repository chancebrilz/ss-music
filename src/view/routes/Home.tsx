import * as Preact from "preact";
import { AlbumSearch } from "../components";

const DEFAULT_SEARCH_TERM = "lake street dive";

export const Home = () => (
  <>
    <div class="welcome">
      <p>Hi! 👋</p>
      <p>
        Welcome to my music app! You can use the following input to search for
        albums by artist name. I've prefilled it with name of one of my favorite
        artists to get you started.
      </p>
      <p>Click on an album to learn more.</p>
    </div>
    <AlbumSearch term={DEFAULT_SEARCH_TERM} />
  </>
);

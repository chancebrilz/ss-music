import * as Preact from "preact";

export const Error404 = () => (
  <section>
    <h2>Oops!</h2>
    <p>The page or resource you requested could not be found.</p>
  </section>
);

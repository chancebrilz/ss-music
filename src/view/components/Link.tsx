import * as Preact from "preact";
import { Link as RouterLink, LinkProps } from "preact-router/match";

export const Link: Preact.FunctionComponent<LinkProps> = ({
  href,
  children,
}) => (
  <RouterLink href={href} activeClassName="active">
    {children}
  </RouterLink>
);

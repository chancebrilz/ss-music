import * as Preact from "preact";
import { Link } from "preact-router";
import { AlbumArt } from "./AlbumArt";

import type { Album } from "~/types";

export const AlbumCard = ({ album }: { album: Album }) => {
  return (
    <Link class="album-card" href={`/album/${album.collectionId}`}>
      <AlbumArt class="album-card--img" album={album} />
      <div class="album-card--name">{album.collectionName}</div>
      <div class="album-card--artist">{album.artistName}</div>
    </Link>
  );
};

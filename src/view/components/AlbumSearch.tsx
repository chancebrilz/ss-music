import * as Preact from "preact";
import { useCallback, useEffect, useState } from "preact/hooks";
import { useMusicApi } from "~/hooks";
import { AlbumCard } from "./AlbumCard";

import type { Album } from "~/types";

const LS_KEY = "ss-music__as-term";

export const AlbumSearch = ({ term }: { term?: string }) => {
  const do_search = useMusicApi("search");
  const cached = localStorage.getItem(LS_KEY) ?? term;

  const [state, setState] = useState<{
    term: string;
    albums: Album[] | undefined;
  }>({
    term: cached,
    albums: undefined,
  });

  useEffect(() => {
    do_search({ term: state.term, entity: "album", limit: 12 }).then(result => {
      if (result.success) {
        const results = result.response["results"];
        const albums = Array.isArray(results) ? results : [];

        setState(s => ({ ...s, albums }));
        return;
      }

      // TODO: Else, display error page.
    });
  }, [state.term]);

  const onTermInput = useCallback(
    e => {
      const term = e.target.value;
      localStorage.setItem(LS_KEY, term);
      setState(s => ({ ...s, term }));
    },
    [setState]
  );

  return (
    <div class="album-search">
      <label class="sr-only" for="album-search-input">
        Artist Name
      </label>
      <input
        id="album-search-input"
        class="album-search--input"
        type="string"
        value={state.term}
        onInput={onTermInput}
        placeholder="Enter an artist name..."
      />
      {state.term && state.albums?.length === 0 ? (
        <span class="hint">No results found.</span>
      ) : null}
      {state.albums ? (
        <div class="album-search--results">
          {state.albums.map(album => (
            <div class="album-search--item">
              <AlbumCard album={album} />
            </div>
          ))}
        </div>
      ) : (
        <div class="loading">Loading...</div>
      )}
    </div>
  );
};

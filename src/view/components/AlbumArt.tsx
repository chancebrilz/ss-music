import * as Preact from "preact";
import { Album } from "~/types";

// Helper component that upscales the available artwork.
export const AlbumArt: Preact.FunctionComponent<{
  album: Album;
  class?: string;
}> = ({ album, class: _class }) => {
  const high_res = album.artworkUrl100.replace("100x100", "300x300");
  return (
    <img
      src={high_res}
      alt={`${album.collectionName} album cover`}
      class={`${_class} album-art`}
    />
  );
};

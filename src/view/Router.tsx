import * as Preact from "preact";
import { Router as PreactRouter, Route } from "preact-router";
import { Home, AlbumDetail, Error404 } from "~/view";

export const Router = () => {
  return (
    <PreactRouter>
      <Route path="/" component={Home} />
      <Route path="/album/:id" component={AlbumDetail} />
      <Route default component={Error404} />
    </PreactRouter>
  );
};

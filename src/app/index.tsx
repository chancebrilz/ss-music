import * as Preact from "preact";
import { Router, Link } from "~/view";

const App = () => (
  <div class="container">
    <header class="site-header">
      <h1 class="site-title">
        <Link href="/">SS Music</Link>
      </h1>
      <nav class="site-nav">
        <ol>
          <li>
            <Link href="/">Home</Link>
          </li>
        </ol>
      </nav>
    </header>
    <main>
      <Router />
    </main>
  </div>
);

const container = document.getElementById("app");

Preact.render(<App />, container);

import { useCallback } from "preact/hooks";
import { make_music_api } from "~/api";

import { MusicAPI, MusicAPIResult } from "~/types";

type APIOperation = keyof MusicAPI;

type APIParameters<Operation extends APIOperation> = Parameters<
  MusicAPI[Operation]
>[0];

export const useMusicApi = <
  Operation extends APIOperation,
  Params extends APIParameters<Operation>
>(
  operation: Operation
) =>
  useCallback(
    (params: Params): Promise<MusicAPIResult> => {
      // @ts-ignore
      return make_music_api()[operation](params);
    },
    [operation]
  );

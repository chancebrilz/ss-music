# SS Music

This repository includes a music app that serves as a trial project.

## Prerequisites

This project requires Node version 12 (or higher) running on a Unix-like system.

You will need Docker installed to build and serve the app.

## Quick start

You can get up and running in one step by using the serve script:

```
npm run serve
```

This will build a Docker image, copy the source, build, and serve.

You can define which port gets mapped by specifying the `PORT` environment
variable. The default value is 3000. Example: `PORT=4123 npm run serve`

## Local development

This project uses the [parcel bundler](https://parceljs.org/). You can start a
local development server by running the start script:

```
npm run start
```

## Configuration

This application currently does not recognize any externally-defined build
settings.

## Code formatting

Consistent code formatting helps us avoid wasting time. Files in this codebase
should use 2 spaces for indentation.

TypeScript and JSON source files use [prettier](https://prettier.io/) as a
formatter. If you use editor integration to get format-on-save (recommended),
then no additional steps are necessary. But you can also run `npm run format`
before committing to validate.

## A note on JSONP

The [API wrapper](src/api/music.ts) depends on JSONP instead of calling the API
directly. During development it became apparent the
[iTunes Store API](https://affiliate.itunes.apple.com/resources/documentation/itunes-store-web-service-search-api/)
does not always return a valid `Access-Control-Allow-Origin` header. The
previously linked doc touches on this in the "Searching" section.

## The future

The immediate next step for this project is adding tests:

- test music api wrapper
- test reusable components
- test for accessibility compliance using an automated tool like Axe

There are a couple developer focused quality of life improvement that could be
made:

- auto stop old Docker container when running the serve command
- enable hot module replacement

Additional improvements that can be made as time allows:

- break up styles
- improve api error handling; display errors to use when appropriate
